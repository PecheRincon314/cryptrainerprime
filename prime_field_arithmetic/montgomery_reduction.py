from prime_field_arithmetic.inverse_mod_p import euclid_extended_algorithm


class ErrorGCD(Exception):
    """Exception raised for errors in the input for gcd montgomery_reduction function.

    Attributes:
        m -- input module which caused the error
        b -- input radix which caused the error
        message -- explanation of the error
    """

    def __init__(self, m, b, message="!= 1"):
        self.m = m
        self.b = b
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f'gcd({self.m}, {self.b}) {self.message}'


class ErrorT(Exception):
    """Exception raised for errors in the input for t value of montgomery_reduction function.

    Attributes:
        t -- input module which caused the error
        message -- explanation of the error
    """

    def __init__(self, t, message="do not meet the condition"):
        self.t = t
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f'0 <= {self.t} < m*r {self.message}'


def montgomery_reduction(m: int, radix: int, t: int) -> int:
    """

    :param m: Montgomery reduction modulus
    :param radix: The base of a system of numeration
    :param t: non-negative integer
    :return: Montgomery reduction of T modulo m with respect to R

    conditions: 0 <= t < m * R  and gcd(m,b) = 1
    """
    gcd = euclid_extended_algorithm(m, radix)
    if gcd[0] != 1:
        raise ErrorGCD(m, radix)
    n = len(str(m))
    r = radix ** n
    if t < 0 or t >= (m * r):
        raise ErrorT(t)

    m_minus_1 = pow(m, -1, radix)
    m_quote = (-m_minus_1) % radix

    a_sub_i = list(str(t)[::-1])

    for i in range(n):
        u_sub_i = (int(a_sub_i[i]) * m_quote) % radix
        a = 0
        for j in range(len(a_sub_i)):
            a += int(a_sub_i[j]) * (radix ** j)
        a += (u_sub_i * m * (radix ** i))
        a_sub_i = list(str(a)[::-1])

    a = int(a / (radix ** n))

    if a >= m:
        a -= m

    return a


if __name__ == '__main__':
    test = montgomery_reduction(72639, 10, 7118368)
    print(test)
