from prime_field_arithmetic.multi_precision_addition import multi_precision_addition
from prime_field_arithmetic.generating_random_prime import generate_prime


def add(number_one: int, number_two: int, prime_number: int) -> int:
    """
    Función que realiza la operación *(a + b) mod p* utilizando el operador % de Python.

    :param number_one: integer number
    :param number_two: integer number
    :param prime_number: prime number
    :return: general modular addition
    """
    return ((number_one % prime_number) + (number_two % prime_number)) % prime_number


def addition_in_fp(a: int, b: int, prime: int, radix: int) -> int:
    """
    Función que realiza la operación *(a + b) mod p* utilizando la suma de precisión múltiple.
    Se requiere que a y b sean enteros positivos con a, b < p.

    :param a: non-negative integer
    :param b: non-negative integer
    :param prime: prime number
    :param radix: the base of a system of numeration.
    :return: modular addition
    """
    a = str(a)
    b = str(b)
    if len(a) > len(b):
        b = '0' * (len(a) - len(b)) + b
    elif len(a) < len(b):
        a = '0' * (len(b) - len(a)) + a
    c = multi_precision_addition(a, b, radix, len(a))
    if c >= prime:
        c = c - prime
    return c


if __name__ == '__main__':
    p = generate_prime(1024)
    addition = add(2 ** 200, 2 ** 300, p)
    print(f'prime numer = {p}\nsuma = {addition}')
    addition_fp = addition_in_fp(2 ** 200, 2 ** 300, p, 10)
    print(f'addition_fp = {addition_fp}')
