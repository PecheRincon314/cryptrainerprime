from prime_field_arithmetic.multiple_precision_division import multiple_precision_division
from prime_field_arithmetic.multi_precision_multiplication import multi_precision_multiplication
from prime_field_arithmetic.generating_random_prime import generate_prime


def multiplication(number_one: int, number_two: int, prime_number: int) -> int:
    """
    Función que realiza la operación (a * b) mod p utilizando el operador % de Python.

    :param number_one: integer number
    :param number_two: integer number
    :param prime_number: prime number
    :return: general modular multiplication
    """
    return ((number_one % prime_number) * (number_two % prime_number)) % prime_number


def classical_modular_multiplication(x: int, y: int, prime: int, radix: int) -> int:
    """
    Función que realiza la operación de multiplicación modular clásica. requisitos: el número de dígitos en la
    multiplicación x * y debe ser mayor o igual que el número de dígitos del número primo.

    :param x: non-negative integer
    :param y: non-negative integer
    :param prime: prime number
    :param radix: The base of a system of numeration
    :return: modular subtraction of two positive integers
    """
    product = multi_precision_multiplication(x, y, radix)
    quotient, remainder = multiple_precision_division(product, prime, radix)
    return remainder


def montgomery_multiplication(m: str, x: str, y: str, b: int) -> int:
    """
    Función que realiza la operación de multiplicación mod p de Montgomery entre dos números enteros postivos.
    Requisitos: gcd(m, b)=1

    :param m: Montgomery multiplication modulus
    :param x: non-negative integer m > x >= 0
    :param y: non-negative integer m > y >= 0
    :param b: radix of number system
    :return: xyR−1 mod m.
    """
    if len(m) > len(x):
        x = '0' * (len(m) - len(x)) + x
    x_sub_i = list(x[::-1])
    y_sub_i = list(y[::-1])
    n = len(m)
    m = int(m)
    m_minus_1 = pow(m, -1, b)
    m_quote = (-m_minus_1) % b
    a_sub_i = [0] * (n + 1)
    for i in range(n):
        u_sub_i = ((int(a_sub_i[0]) + (int(x_sub_i[i]) * int(y_sub_i[0]))) * m_quote) % b
        a = 0
        for j in range(len(a_sub_i)):
            a += int(a_sub_i[j]) * (b ** j)
        a = (a + (int(x_sub_i[i]) * int(y)) + (u_sub_i * m)) // b
        a_sub_i = list(str(a)[::-1])
    if a >= m:
        a -= m
    return a


if __name__ == '__main__':
    # p = generate_prime(1024)
    # mult = multiplication(2 ** 200, 2 ** 300, p)
    # print(f'prime numer = {p}\nmultiplicacion = {mult}')
    # mult2 = classical_modular_multiplication(2 ** 200, 2 ** 199, p, 10)
    # print(f'mult2 = {mult2}')
    x_test = '74106370990897111733726697610291693740084871859972769535694796516080724611830'
    y_test = '124319'
    m_test = '27413366002820230322072907355559122582516678742370207929685671695212700808303151287019156616423039212980145922229823610952432233625831458475095402409103473551393007531153828418020672534081127625755694875659693569902143855798574801379946802472887062304423746764962290910345192388589018924955811319748569908569424344288416765909599187285235154341512974945101833428000332672361167318805525666105548632965546426898629049846851807364287897619584023274148502253610067589211387825098204670049432541337058945013020546749849495939011534174762172031299633826967292511187902996718032704849546819371630507805655746930204038231093'

    # montgomery_test = montgomery_multiplication(m_test, x_test, y_test, 10)
    # montgomery_test = montgomery_multiplication('72639', '5792', '1229', 10)
    montgomery_test = montgomery_multiplication(m_test, x_test, '22212189453087667137062644934808980254436993994827843897617604553595133913549197834354454414904764145864526261940221470404164302610777079746134760984053387989804105832228188557221763454585754950491940734514597186988994971857362806331428411730581348336354308242542242694564603431288106314533011354617486575330340865646683333966754805131719486436859034665433878287740247434186834780237024769662896171071186858656678965103517214670341850860612863970076066815398318013412546843016005614304555466347453851765993225613467428626267953608801231488117503600602707080335327981835656788981919034266970765102921607664752575279336', 10)
    print(montgomery_test)
